const template =
    `
    <table>
    <thead>
    <tr>
      <th>feedbackId</th>
      <th>studentId</th>
      <th>studentName</th>
      <th>trainerId</th>
      <th>trainerName</th>
      <th>feedbackRating</th>
      <th>feedbackComment</th>
     </tr>
     </thead>
     <tbody id="feedback-body">
     </tbody>
     </table>
     `;

const getfeedbackTable = async (token) => {
    document.innerHTML = template;
    try {
        let response = await fetch("https://7c33-103-176-135-84.in.ngrok.io/admin/getFeedback",
            {
                method: "GET",
                headers: new Headers({
                    "Authorization": `Bearer ${token}`,
                    "ngrok-skip-browser-warning": "3200",
                    'Content-Type': 'application/json',
                    'charset': 'utf-8',
                })
            });
        const data = await response.JSON();
        console.log(data);
        console.log(response);
        let row = '';
        for (let feed of data) {
            row += tableRow(feed);
        }
        document.getElementById("feedback-body").innerHTML = row;

    }
    catch (error) {
        console.log(error);
    }

}


const tableRow = (feed) => {
    return `
      <tr>
      <td>${feed.feedbackId}</td>
      <td>${feed.studentId}</td>
      <td>${feed.studentName}</td>
      <td>${feed.trainerId}</td>
      <td>${feed.trainerName}</td>
      <td>${feed.feedbackRating}</td>
      <td>${feed.feedbackComment}</td>
      <td><button class="editButton">Edit</button>
      <button class="deleteButton">Delete</button></td>
      <tr>
  
`
}


export default { getfeedbackTable };









