const template =
    `
        <table>
        <thead>
        <tr>
          <th>domainId</th>
          <th>domainName</th>
          <th>assigned</th>
         </tr>
         </thead>
         <tbody id="domain-body">
         </tbody>
         </table>
         `;


const getDomainsTable = async (token) => {
    document.body.innerHTML = template;
    try {
        let response = await fetch("https://7c33-103-176-135-84.in.ngrok.io/admin/getDomains?=",
            {
                method: "GET",
                headers: new Headers({
                    "Authorization": `Bearer ${token}`,
                    "ngrok-skip-browser-warning": "3200",
                    'Content-Type': 'application/json',
                    'charset': 'utf-8',
                })
            });
        const data = await response.JSON();
        console.log(data);
        console.log(response);
        let row = '';
        for (let domain of data) {
            row += tableRow(domain);
        }
        document.getElementById("domain-body").innerHTML = row;

    }
    catch (error) {
        console.log(error);
    }

}

const tableRow = (domain) => {
    return `
          <tr>
          <td>${domain.domainId}</td>
          <td>${domain.domainName}</td>
          <td>${domain.assigned}</td>
          <td><button class="editButton">Edit</button>
          <button class="deleteButton">Delete</button></td>
          <tr>
      
    `
}


export default { getDomainsTable };







