import getStudentsTable from "./getStudents.js";
import getDomains from "./getDomains.js";
import getFeedback from "./getFeedback.js";

function displayDashboard(token){
    let template =
    `
            <section>
            <h1>DASH BOARD</h1>
             <button class="studentsButton">Students</button>
             <button class="domainsButton">Domains</button>
             <button class="feedbackButton">Feedback</button>
             </section>

             `
document.getElementsByClassName("main")[0].innerHTML = template;

const comment = document.getElementsByClassName("feedbackButton");
const stream = document.getElementsByClassName("domainsButton");   
const students = document.getElementsByClassName("studentsButton");


    stream[0].addEventListener('click', (e)=>{
        e.preventDefault();
        console.log("HII");
        getDomains();

    });
    students[0].addEventListener('click', (e)=>{
        e.preventDefault();
        console.log("students");
        getStudentsTable();
    });
    comment[0].addEventListener('click', (e)=>{
        e.preventDefault();
        getFeedback();
    });

}
    

export default displayDashboard();